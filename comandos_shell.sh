
# PORQUE LUEGO NO SE QUEDAN EN EL HISTORY Y AHÍ ANDA UNO LAMENTÁNDOSE

# MONTAGES CON FOTOS DE TAMAÑOS NO IDÉNTICOS, LES PONE EL NOMBRE COMO ANNOTATION
$ for i in mangle_*; do convert $i -gravity center -crop 650x610+0+0 $i; done
$ for i in mangle_*; do convert $i -fill white -stroke black -pointsize 49 -strokewidth 1 -annotate +80+40  $(echo $i | sed "s/mangle_\|.png//g") $i; done
$ montage mangle_* -geometry 420x420+4+4 mangles_2.png




# UN GPKG DE TODOS LOS VIDEOS:
exiftool -r . -ext MP4  -filename -gpslatitude -gpslongitude -createdate -c %06f -csv > exif_videos_dron.csv
cat exif_videos_dron.csv | sed "s/ N\| W//g" | sed "s/,9/,-9/g" > exif_coords_clean.csv # SÍ MUY HACKY PONER -9 ASÍ
ogr2ogr -f GPKG exif_vids_anafi.gpkg -oo X_POSSIBLE_NAMES=GPSLongitude -oo Y_POSSIBLE_NAMES=GPSLatitude -a_srs EPSG:4326 exif_coords_clean_anafi.csv

# PONERLES CRÉDITOS Y BAJAR TAMAÑO DE FOTOS
# PRIMERO DEBE IR RESIZE PORQUE SI NO EL POINTSIZE SE REFERENCIA A LA IMAGEN GRANDE
mogrify -resize "1000000@>" -stroke '#000C' -strokewidth 2 -pointsize 24  -annotate +15+15 'Pronatura Veracruz, 2021'  *.jpg


# pues bueno, hay que ponerle el año de la foto... a qué costo?
for i in *.JPG; do filenew=$(echo $i | sed "s/^/low_/"); convert -resize "1000000@>" -fill white -stroke black -pointsize 34 -strokewidth 1 -annotate +20+40 "$(printf "\n Pronatura Veracruz, %s" $(exiftool $i  -createdate -d %Y | perl -pe 's/.*(?=20)//'))" $i $filenew; done

-stroke black -pointsize 34 -strokewidth 1 -annotate +20+40 "$(echo $i | sed "s/laguna_//g"))"

#para batch-convertir png a pdf
for i in *.png;
do filename=$(echo $i | sed "s/png/pdf/g") ;
convert $i  -units pixelsperinch -density 72 -page letter $filename;
done 

#para extraer las páginas de un pdf en formato png
pdftoppm Cartas_De_Apoyo_llenadas.pdf -png cartas   

# encima un png a un video DEL MISMO TAMAÑO (para marcos o transparencias)
ffmpeg -i aves.mp4 -i aargradient_range3.png -filter_complex "[0:v][1:v] overlay=0:0" -c:a copy aves_ffmpeg_sun.mp4

# FFMPEG ESCALA Y PONE LOGO 

ffmpeg  -i P1540728.MP4 -i logo_prona.png -filter_complex "[0:v]scale=iw/2:ih/2[inner];[1:v]scale=iw/2:ih/2[logo];[inner][logo]overlay=7:7[out]" -map "[out]" -b:v 2000k OUTPUT.MP4

# VECTORIZADO !!! cambiar el argumento en sed !!!!
for i in *.MP4; do filenew=$(echo $i | sed "s/^P/low_P/g"); ffmpeg  -i $i -i logo_prona.png -filter_complex "[0:v]scale=iw/2:ih/2[inner];[1:v]scale=iw/2:ih/2[logo];[inner][logo]overlay=7:7[out]" -map "[out]" -b:v 2000k $filenew; done

# vamos a ponerle coordenadas impresas a las fotos :)
for i in *.JPG; do filenew=$(echo $i | sed "s/^/low_/"); convert -resize "1000000@>" -fill white -stroke black -pointsize 34 -strokewidth 1 -annotate +20+40 "$(printf "\n %s" $(exiftool $i  -gpslatitude -gpslongitude -relativealtitude -gimbalyawdegree -c %.6f | sed "s/ //g"  ))" $i $filenew; done

exiftool DJI* -relativealtitude -gimbalyawdegree -gpslatitude -gpslongitude -c %.6f -csv | sed "s/ N\| W//g"  | sed "s/,95/,-95/g" > coords_signed.csv

# no pude con vsistdin
ogr2ogr -f kml macuil.kml coords_signed.csv -oo X_POSSIBLE_NAMES=GPSLongitude -oo Y_POSSIBLE_NAMES=GPSLatitude -a_srs EPSG:4326

# consulta con stdin y stdout y buen manejo de geometrías
ogr2ogr -f CSV /vsistdout/ -dialect indirect_sqlite -sql "select zona, zona_num, round(area(transform(geometry,32614)),1) as area_m2 from zonficacion_jalapasco" zonficacion_jalapasco.gpkg | ogrinfo CSV:/vsistdin/ -dialect sqlite -sql "select * from layer"

# para sacar la anterior imprimiendo a stdout con un print tabulado

ogr2ogr -f CSV /vsistdout/ -dialect indirect_sqlite -sql "select zona, zona_num, round(area(transform(geometry,32614)),1) as area_m2 from zonficacion_jalapasco" zonficacion_jalapasco.gpkg | column -s, -t


