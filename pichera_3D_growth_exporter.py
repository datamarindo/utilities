i = 14
from PyQt5.QtCore import QSize
from Qgis2threejs.export import ThreeJSExporter, ImageExporter    # or ModelExporter
from Qgis2threejs.mapextent import MapExtent
import time
import json

# ALTAMENTE IMPORTANTE QUE ESTÉ MONTADO EL DISCO DONDE ESTÁ EL MOSAICO UwU
# los **with** en python... en un FOR no se separan

# para prenderlas y apagarlas
# settings["LAYERS"][3]["visible"]


with open("/home/pronatura/estrategia_fuego/pichera.qgz.qto3settings", "r", encoding="utf-8") as f:
	settings = json.load(f)	

# arboles_modelo_escuadrona
settings["LAYERS"][4]["properties"]["geomWidget0"]["editText"] = "rand({0},{1})*0.001".format( ((i < 8)*i)+(8*(i>=9)) , ((i < 9)*i)+(9*(i>=9)) )
# reclutamiento 10
settings["LAYERS"][3]["properties"]["geomWidget0"]["editText"] = "rand({0},{1})*0.0013".format( (i>7)*(i-7) , (i>6)*(i-6))
# reclutamiento_year_7
settings["LAYERS"][5]["properties"]["geomWidget0"]["editText"] = "rand({0},{1})*0.0013".format( (i>5)*(i-5) , (i>4)*(i-4))
# reclutamiento
settings["LAYERS"][6]["properties"]["geomWidget0"]["editText"] = "rand({0},{1})*0.0012".format( (i>3)*(i-3) , (i>2)*(i-2))
# el año
settings["WIDGETS"]["Label"]["Header"] = '<b>Diseño de restauración de manglar <br> para manejo de fuego, año %s </b><br><img src="/home/pronatura/oh_my_gits/croquis_predios/logo_prona.png" width=100>'% (i)
with open('/home/pronatura/pichis.qgz.qto3settings', 'w', encoding='utf-8') as f:
	 json.dump(settings, f, ensure_ascii=False, indent=4)

#def generar(i):
# texture base size
TEX_WIDTH, TEX_HEIGHT = (1024, 1024)
path_to_settings = '/home/pronatura/pichis.qgz.qto3settings'
mapSettings = iface.mapCanvas().mapSettings()
# extent to export
center = mapSettings.extent().center() # QgsPointXY(208703, 2056137)
width =  400 #mapSettings.extent().width() # estos valores están variando con 
height = width * TEX_HEIGHT / TEX_WIDTH
rotation = 120
MapExtent(center, width, height, rotation).toMapSettings(mapSettings)
# texture base size
mapSettings.setOutputSize(QSize(TEX_WIDTH, TEX_HEIGHT))
filename = "picheracreciendo_%02d.png" % i
CAMERA = {'pos': {'x': 208703, 'y': 2056137, 'z': 130}, 'lookAt': {'x': 208501, 'y': 2056004, 'z': 11}}
exporter = ImageExporter()
exporter.loadSettings(path_to_settings)
exporter.setMapSettings(mapSettings)
exporter.initWebPage(1024, 768)                       # output image size
exporter.export(filename, cameraState=CAMERA)
#time.sleep(2)
exporter.export(filename, cameraState=CAMERA)


